<p align="center">
  <img src="https://i.ibb.co/yRkQVnm/203605945-beautiful-girl-with-long-hair-in-fairy-forest-3d-rendering.jpg">
</p>

<h4><p align="center"> Emily </p></h4>

<p align="center">Emily is a modular bot running on python3.9 with autobots theme and have a lot features.</p>

<p align="center">
<a href="https://t.me/dEmily_Bot"> <img src="https://img.shields.io/badge/dEmily_Bot-blue?&logo=telegram" alt="Emily on Telegram" /> </a><br>
<a href="https://python-telegram-bot.org"> <img src="https://img.shields.io/badge/PTB-13.9.0-white?&style=flat-round&logo=github" alt="Python Telegram Bot" /> </a>
<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a><br>
<a href="https://docs.telethon.dev"> <img src="https://img.shields.io/badge/Telethon-1.24.0-red?&style=flat-round&logo=github" alt="Telethon" /> </a>
<a href="https://docs.python.org"> <img src="https://img.shields.io/badge/Python-3.9-purple?&style=flat-round&logo=python" alt="Python" /> </a><br>
<a href="https://gitlab.com/nickylalrochhara/Emily"> <img src="https://img.shields.io/badge/Maintained-Yes-yellow.svg" alt="Maintenance" /> </a><br>
<a href="https://gitlab.com/nickylalrochhara/Emily/blob/main/LICENSE"> <img src="https://img.shields.io/badge/License-GPLv3-blue.svg" alt="License" /> </a>
<a href="https://makeapullrequest.com"> <img src="https://img.shields.io/badge/PRs-Welcome-blue.svg?style=flat-round" alt="PRs" /> </a>
</p>

# Deployment on Heroku or VPS

<details>
<summary><b> 🚀 Heroku Deployment</b></summary>
<br>
<h4>Click the button below to deploy Emily on Heroku!</h4>    
<a href="https://heroku.com/deploy?template=https://gitlab.com/nickylalrochhara/Emily"><img src="https://img.shields.io/badge/Deploy%20To%20Heroku-blueviolet?style=for-the-badge&logo=heroku" width="200""/></a>

</details>

<details>
<summary><b>🔗 Deploy on VPS</b></summary>
<br>
    
### Tutorial Deploy on VPS
 
```console
root@dEmily_Bot~ $ screen -S dEmily_Bot
root@dEmily_Bot~ $ git clone https://gitlab.com/nickylalrochhara/Emily
root@dEmily_Bot~ $ cd dEmily_Bot
root@dEmily_Bot~ $ pip3 install -U -r requirements.txt
root@dEmily_Bot~ $ cp Emily.env .env
root@dEmily_Bot~ $ nano .env
root@dEmily_Bot~ $ bash start
```

</details>

### Contributors
- [ Nicky Lalrochhara ](https://gitlab.com/nickylalrochhara) : Emily
- [『TØNIC』 乂 ₭ILLΣR](https://github.com/Tonic990) : Emily Devs
- [Arya](https://github.com/aryazakaria01) : Natsunagi Devs
- [Xbarox](https://github.com/Xbaroxx)

### Credit
- Cutiepii
- Skyzu Robot
- Emiko Robot
- Natsunagi Nagisa

import random
from Emily.events import register
from Emily import telethn

is_STRING = ["Yes",
                 "No",
                 "Maybe",
                 "Maybe not",
                 "Could be",
                 "Possibly not",
                 "Unlikely",
                 "You never truly know the situation",
                 "Your father's head is broken",
                 "Is it?",
                 "Ask your mother, you idiot",
                 "You ask me, then I ask who?"
                 ]

@register(pattern="^/is ?(.*)")
async def is(event):
    question = event.pattern_match.group(1)
    if not question:
        await event.reply('Give me a question 😐')
        return
    await event.reply(random.choice(is_STRING))
